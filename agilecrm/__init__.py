#!/usr/bin/python

''' More info about the AgileCrm API:  https://github.com/agilecrm/rest-api '''
import json
import requests


class DuplicateError(Exception):
        pass


class UnknownContentType(Exception):
        pass


class AgileCrm:
    def __init__(self, email, apikey, domain):
        self.email = email
        self.apikey = apikey
        self.domain = domain
        self.startpoint = 'https://' + self.domain + '.agilecrm.com/dev/api/'

    def agile_url(self, endpoint):
        return self.startpoint + endpoint

    def get(self, endpoint, data=None):
        headers = {'Accept': 'application/json'}
        return requests.get(self.agile_url(endpoint), headers=headers,
                            auth=(self.email, self.apikey))

    def post(self, endpoint, data, content_type='application/json'):
        # headers = {'Accept': 'application/json'}
        headers = {'Content-Type': content_type,
                   'Accept': 'application/json'}
        if content_type == 'application/json':
            return requests.post(self.agile_url(endpoint), headers=headers,
                                 data=json.dumps(data),
                                 auth=(self.email, self.apikey))
        elif content_type == 'application/x-www-form-urlencoded':
            return requests.post(self.agile_url(endpoint), headers=headers,
                                 data=data,
                                 auth=(self.email, self.apikey))
        else:
            raise UnknownContentType('%s is unkown' % (content_type))

    def patch(self, endpoint, data):
        headers = {'Content-Type': 'application/json'}
        return requests.patch(self.agile_url(endpoint), headers=headers,
                              data=json.dumps(data),
                              auth=(self.email, self.apikey))

    def put(self, endpoint, data):
        headers = {'Content-Type': 'application/json'}
        return requests.put(self.agile_url(endpoint), headers=headers,
                            data=json.dumps(data),
                            auth=(self.email, self.apikey))

    def delete(self, endpoint, data=None):
        headers = {'Content-Type': 'application/json'}
        if data is None:
            return requests.delete(self.agile_url(endpoint), headers=headers,
                                   auth=(self.email, self.apikey))
        else:
            return requests.delete(self.agile_url(endpoint), headers=headers,
                                   data=json.dumps(data),
                                   auth=(self.email, self.apikey))

    def encode_contact(self, contact):
        ''' Convert the simple dict contact to the obscure contact
            format from agilecrm
        contact = {'tags': ['some tag', 'another tag'],
                   'properties': [{'type': 'SYSTEM',
                                   'name': 'first_name',
                                   'value': 'John',},
                                  {'type': 'SYSTEM',
                                   'name': 'last_name',
                                   'value': 'Smith',}]
                                  {'type': 'SYSTEM',
                                   'name': 'company',
                                   'value': 'Business ltd',}]
                                  {'type': 'SYSTEM',
                                   'name': 'email',
                                   'value': 'j.smith@business.co.uk',}]}
        '''
        out = {}
        out['properties'] = []
        for k in contact:
            try:
                if k == 'tags' or k == 'id':
                    out[k] = contact[k]
                else:
                    p = {}
                    p['type'] = 'SYSTEM'
                    p['name'] = k
                    p['value'] = contact[k]
                    out['properties'].append(p)
            except KeyError:
                pass
        return out

    def decode_contact(self, contact):
        ''' Convert the obscure contact format from agilecrm to a
            simple dict '''
        out = {}
        if type(contact) != dict:
            raise TypeError("Cannot decode contact, input is not a dictionary")

        for k in contact:
            try:
                if k != 'properties':
                    out[k] = contact[k]
                else:
                    for kk in contact[k]:
                        out[kk['name']] = kk['value']
            except KeyError:
                pass
        return out

    def contacts_list(self):
        # out = []
        # for contact in self.get('contacts').json():
        #     if type(contact) != dict:
        #         raise TypeError("Weird, not getting dicts")
        #     out.append(self.decode_contact(contact))
        # return out
        return self.get('contacts').json()

    def contacts_by_id(self, contact_id):
        # return self.decode_contact(self.get('contacts/' + str(contact_id)))
        return self.get('contacts/' + str(contact_id))

    def contacts_by_email(self, email):
        ''' Should return a contact with the email '''
        content_type = 'application/x-www-form-urlencoded'
        # r = self.post('contacts/search/email',
        #               'email_ids=[%s]' % (email), content_type)
        # contacts = []

        # for c in r.json():
        #    contacts.append(self.decode_contact(c))

        # return contacts
        return self.post('contacts/search/email',
                         'email_ids=[%s]' % (email), content_type).json()

    def contacts_create(self, contact, verify_duplicate=True):
        # Don't go sending empty contacts
        # A new contact can look like this:
        # contact = {'first_name': 'Sascha',
        #            'last_name': 'Dobbelaere',
        #            'company': 'S-Company',
        #            'email': 'info@s-company.be',
        #            'phone': '0044 1134 303030',
        #            }
        if len(contact) == 0:
            raise KeyError('Empty contact')

        # Check for duplicate contacts by email-address
        if verify_duplicate is True:
            if self.contacts_by_email(contact['email'])[0] is not None:
                raise DuplicateError('Contact exists')
        
        # All is good, go for it
        return self.post('contacts', self.encode_contact(contact))

    def contacts_update(self, contact, warning=False):
        # Check if there is an id. Prevent from creating new contacts
        try:
            contact['id']
        except KeyError:
            raise KeyError('Contact id is missing')

        if warning = True:
            print 'This function is expirimental an may return very unexpected results'
            raw_input('Press any key if you understand')
        
        #  Update the contact and return the reply
        # return self.put('contacts', self.encode_contact(contact))
        return self.put('contacts', contact)

    def contacts_add_tags_by_email(self, email, tags):
        # Different content_type needed for this request
        content_type = "application/x-www-form-urlencoded"

        # Return a boolean to verify the creation
        return self.post('contacts/email/tags/add',
                         'email=%s&tags=%s' % (email, tags),
                         content_type).ok

    def contacts_add_score_by_email(self, email, score):
        # Add score to an email-address
        # For substraction, use negative number
        # Returns boolean
        content_type = "application/x-www-form-urlencoded"
        return self.post('contacts/add-score/',
                         'email=%s&score=%s' % (email, score),
                         content_type).ok

    def contacts_delete(self, contact_id):
        # Delete the given id
        return self.delete('contacts/' + str(contact_id))

    def contacts_delete_bulk(self, contact_ids):
        # TODO broken, returs 400 - Bad request
        data = {"model_ids": contact_ids}
        # return self.delete('contacts/bulk', data)
        return 'Broken'

    def contacts_by_tag(self, tag):
        # return a list of contacts that contain the tag
        c_full = self.contacts_list()
        c_filter = []

        # populate the c_filter list
        for c in c_full:
            if tag in c['tags']:
                c_filter.append(c)

        # Return the contacts that are found
        return c_filter

    def note_create_by_email(self, subject, note, email):
        # Compile dict
        d = json.dumps({'subject': subject,
             'description': note})

        # Different content_type needed for this request
        content_type = "application/x-www-form-urlencoded"

        # Return a boolean to verify the creation
        return self.post('contacts/email/note/add',
                         'email=%s&note=%s' % (email, d),
                         content_type).ok
