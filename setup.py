from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='agilecrm',
      version='0.1',
      description='Client for agile crm',
      url='https://scompany@bitbucket.org/scompany/agilecrm.git',
      author='Sascha Dobbelaere',
      author_email='s.dobbelaere@s-company.co.uk',
      license='',
      packages=['agilecrm'],
      install_requires=[
          'requests',
          ],
      zip_safe=False)
