#!/usr/bin/python

import unittest
import agilecrm


class TestContacts(unittest.TestCase):
    def setUp(self):
        email = 's.dobbelaere@s-company.co.uk'
        apikey = 'c93lu7j5h3bg3gg9rk64jtmj63'
        domain = 'scompany'
        self.a = agilecrm.AgileCrm(email=email, apikey=apikey, domain=domain)

    def test_create_user(self):
        contact = {'properties': [{'name': 'first_name',
                                   'type': 'SYSTEM',
                                    'value': 'frtname'},
                                  {'name': 'last_name',
                                   'type': 'SYSTEM',
                                   'value': 'last'}],
                    'tags': ['test-api-upload']}
        r = self.a.contacts_create(contact)
        rr = self.a.contacts_by_id(r.json()['id'])
        self.assertTrue(r.json() == rr.json())


if __name__ == '__main__':
    unittest.main()
